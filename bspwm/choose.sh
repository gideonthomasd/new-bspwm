#!/bin/bash

# options to be displayed
option0="cristina - bottom bar"
option1="cynthia - two bars - top and bottom"
option2="emilia - middle"
option3="karla - 3 bars at top"
option4="pamela - many bars at top"
option5="silvia - Colored boring bar"
option6=" reboot"
option7="⏻ shutdown"

# options passed into variable
options="$option0\n$option1\n$option2\n$option3\n$option4\n$option5\n$option6\n$option7"

chosen="$(echo -e "$options" | rofi -lines 8 -dmenu -p "power" -font "JetBrainsMono Nerd Font 18")"
case $chosen in
    $option0)
		echo "cristina" > ~/.config/bspwm/.rice && ~/.config/bspwm/bspwmrc ;;
		#i3-msg exit;;
		#pkill chadwm || pkill dwm || pkill i3 || pkill bspwm || pkill openbox || pkill herbstluftwm || pkill awesome || pkill qtile || pkill python;;
    $option1)
        echo "cynthia" > ~/.config/bspwm/.rice && ~/.config/bspwm/bspwmrc ;;
    $option2)
        echo "emilia" > ~/.config/bspwm/.rice && ~/.config/bspwm/bspwmrc ;;
	$option3)
		echo "karla" > ~/.config/bspwm/.rice && ~/.config/bspwm/bspwmrc ;;
	$option4)
		echo "pamela" > ~/.config/bspwm/.rice && ~/.config/bspwm/bspwmrc ;;
	$option5)
		echo "silvia" > ~/.config/bspwm/.rice && ~/.config/bspwm/bspwmrc ;;
    $option6)
		#systemctl reboot;;
		#loginctl reboot;;
        sudo reboot;;
	$option7)
		#systemctl poweroff;;
		#loginctl poweroff;;
        sudo poweroff;;
esac
