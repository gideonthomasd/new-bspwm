#!/bin/bash

cp *.otf ~/.local/share/fonts
cp *.ttf ~/.local/share/fonts


mv ~/.config/bspwm ~/.config/bspwm-original
mkdir -p ~/.config/bspwm

cd bspwm
chmod +x bspwmrc
chmod +x audacious.sh
chmod +x choose.sh
chmod +x playerctl.sh

cd scripts
chmod +x Brightness
chmod +x BspHideNode
chmod +x ExternalRules
chmod +x HideBar
chmod +x KeybindingsHelp
chmod +x MediaControl
chmod +x *.sh
chmod +x OpenApps
chmod +x RiceSelector
chmod +x run_jgmenu
chmod +x ScreenShoTer
chmod +x SetSysVars
chmod +x systray
chmod +x Updates
chmod +x Volume
chmod +x WallSelect
chmod +x Weather

cd ..
cp -r * ~/.config/bspwm
cd ..

cd ~/.config/bspwm
mv rice .rice


